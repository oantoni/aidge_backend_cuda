#include "aidge/backend/cuda/utils/CudaUtils.hpp"

const char* Aidge::Cuda::cublasGetErrorString(cublasStatus_t error)
{
    switch (error) {
    case CUBLAS_STATUS_SUCCESS:
        return "CUBLAS_STATUS_SUCCESS";
    case CUBLAS_STATUS_NOT_INITIALIZED:
        return "CUBLAS_STATUS_NOT_INITIALIZED";
    case CUBLAS_STATUS_ALLOC_FAILED:
        return "CUBLAS_STATUS_ALLOC_FAILED";
    case CUBLAS_STATUS_INVALID_VALUE:
        return "CUBLAS_STATUS_INVALID_VALUE";
    case CUBLAS_STATUS_ARCH_MISMATCH:
        return "CUBLAS_STATUS_ARCH_MISMATCH";
    case CUBLAS_STATUS_MAPPING_ERROR:
        return "CUBLAS_STATUS_MAPPING_ERROR";
    case CUBLAS_STATUS_EXECUTION_FAILED:
        return "CUBLAS_STATUS_EXECUTION_FAILED";
    case CUBLAS_STATUS_INTERNAL_ERROR:
        return "CUBLAS_STATUS_INTERNAL_ERROR";
    case CUBLAS_STATUS_NOT_SUPPORTED:
        return "CUBLAS_STATUS_NOT_SUPPORTED";
    case CUBLAS_STATUS_LICENSE_ERROR:
        return "CUBLAS_STATUS_LICENSE_ERROR";
    }

    return "<unknown>";
}

void Aidge::Cuda::setMultiDevicePeerAccess(unsigned int size, unsigned int* devices)
{
    for (unsigned int i = 0; i < size; ++i) {
        for (unsigned int j = 0; j < size; ++j) {
            if (i != j) {
                int canAccessPeer = 0;
                CHECK_CUDA_STATUS(cudaDeviceCanAccessPeer(&canAccessPeer,
                                            devices[j], devices[i]));                     
                if (canAccessPeer) {
                    CHECK_CUDA_STATUS(cudaSetDevice(devices[j]));
                    const cudaError_t status = cudaDeviceEnablePeerAccess(devices[i], 0);
                    if (status == cudaErrorPeerAccessAlreadyEnabled) {
                        fmt::print("Peer access already enabled between device {} and device {}\n", devices[j], devices[i]);
                    } else {
                        CHECK_CUDA_STATUS(status);
                    }
                }
            }
        }
    }
}

/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
#include <stdio.h>

#include "aidge/backend/cuda/operator/FCImpl_CUDA_kernels.hpp"

namespace Aidge{

template <>
cublasStatus_t cublasGemm<__half>(cublasHandle_t handle,
                                  cublasOperation_t transa, cublasOperation_t transb,
                                  int m, int n, int k,
                                  const __half *alpha,
                                  const __half *A, int lda,
                                  const __half *B, int ldb,
                                  const __half *beta,
                                  __half *C, int ldc)
{
    return cublasHgemm(handle,
					   transa, transb,
					   m, n, k,
					   alpha,
					   A, lda,
					   B, ldb,
					   beta,
					   C, ldc);
}

template <>
cublasStatus_t cublasGemm<float>(cublasHandle_t handle,
                                 cublasOperation_t transa, cublasOperation_t transb,
                                 int m, int n, int k,
                                 const float *alpha,
                                 const float *A, int lda,
                                 const float *B, int ldb,
                                 const float *beta,
                                 float *C, int ldc)
{
    return cublasSgemm(handle,
					   transa, transb,
					   m, n, k,
					   alpha,
					   A, lda,
					   B, ldb,
					   beta,
					   C, ldc);
}

template <>
cublasStatus_t cublasGemm<double>(cublasHandle_t handle,
								  cublasOperation_t transa, cublasOperation_t transb,
								  int m, int n, int k,
								  const double *alpha,
								  const double *A, int lda,
								  const double *B, int ldb,
								  const double *beta,
								  double *C, int ldc)
{
    return cublasDgemm(handle,
					   transa, transb,
					   m, n, k,
					   alpha,
					   A, lda,
					   B, ldb,
					   beta,
					   C, ldc);
}
}
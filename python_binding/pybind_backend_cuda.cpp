#include <pybind11/pybind11.h>
// Need to call this header to register every impl
#include "aidge/backend/cuda.hpp"

namespace py = pybind11;

namespace Aidge {

void init_cuda_sys_info(py::module& m);

void init_Aidge(py::module& m){
    init_cuda_sys_info(m);
}

PYBIND11_MODULE(aidge_backend_cuda, m) {
    init_Aidge(m);
}
}

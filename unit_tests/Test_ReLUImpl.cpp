/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <array>

#include <catch2/catch_test_macros.hpp>

#include "Test_cuda.hpp"

#include "aidge/data/Tensor.hpp"

#include "aidge/backend/cpu.hpp"
#include "aidge/backend/cuda.hpp"

using namespace Aidge;


TEST_CASE("[gpu/operator] ReLU(forward)", "[ReLU][GPU]") {
    SECTION("1D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array1D<float,10> {
            {0, 1, 2,-3, 4,-5,-6, 7, 8, 9}
        });
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array1D<float,10> {
            {0, 1, 2, 0, 4, 0, 0, 7, 8, 9}
        });

        std::shared_ptr<Node> myReLU = ReLU();
        auto op = std::static_pointer_cast<OperatorTensor>(myReLU -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        myReLU->forward();

        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("2D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array2D<float,2,10> {
            {
                { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
            }
        });
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array2D<float,2,10> {
            {
                { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
            }
        });

        std::shared_ptr<Node> myReLU = ReLU();
        auto op = std::static_pointer_cast<OperatorTensor>(myReLU -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        myReLU->forward();
        
        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("3D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array3D<float,2,2,10> {
            {
                {
                    { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                    {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                },
                {
                    { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                    {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                }
            }
        });
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array3D<float,2,2,10> {
            {
                {
                    { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                    { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                },
                {
                    { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                    { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                }
            }
        });

        std::shared_ptr<Node> myReLU = ReLU();
        auto op = std::static_pointer_cast<OperatorTensor>(myReLU -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        myReLU->forward();

        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("4D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array4D<float,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                },
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                }
            }
        });
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    },
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    }
                },
                {
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    },
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    }
                }
            }
        });

        std::shared_ptr<Node> myReLU = ReLU();
        auto op = std::static_pointer_cast<OperatorTensor>(myReLU -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        op->forward();

        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }
}

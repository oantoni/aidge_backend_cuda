/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <array>

#include <catch2/catch_test_macros.hpp>
#include <cuda_fp16.h>
#include <numeric>   // std::accumulate
#include <random>    // std::random_device, std::mt19937, std::uniform_real_distribution

#include "Test_cuda.hpp"

#include "aidge/data/half.hpp"
#include "aidge/data/Tensor.hpp"

#include "aidge/backend/cpu.hpp"
#include "aidge/backend/cuda.hpp"

using namespace Aidge;

TEST_CASE("[gpu/operator] AvgPooling(forward)", "[AvgPooling][GPU]") {
    std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<float,2,2,5,5> { //NCHW
        {
            {
                {{  0,   1,   2,   3,   4},
                 {  5,   6,   7,   8,   9},
                 { 10,  11,  12,  13,  14},
                 { 15,  16,  17,  18,  19},
                 { 20,  21,  22,  23,  24}},

                {{ 25,  26,  27,  28,  29},
                 { 30,  31,  32,  33,  34},
                 { 35,  36,  37,  38,  39},
                 { 40,  41,  42,  43,  44},
                 { 45,  46,  47,  48,  49}}
            },
            {
                {{100, 101, 102, 103, 104},
                 {105, 106, 107, 108, 109},
                 {110, 111, 112, 113, 114},
                 {115, 116, 117, 118, 119},
                 {120, 121, 122, 123, 124}},

                {{125, 126, 127, 128, 129},
                 {130, 131, 132, 133, 134},
                 {135, 136, 137, 138, 139},
                 {140, 141, 142, 143, 144},
                 {145, 146, 147, 148, 149}}
            }
        }
    });
    SECTION("Stride") {
        std::shared_ptr<Node> myAvgPool = AvgPooling({2,2}, "myAvgPool", {2,2});
        auto op = std::static_pointer_cast<OperatorTensor>(myAvgPool -> getOperator());

        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float,2,2,2,2> {
            {
                {
                    {{  3,   5},
                     { 13,  15}},
                    {{ 28,  30},
                     { 38,  40}}
                },
                {
                    {{103, 105},
                     {113, 115}},
                    {{128, 130},
                     {138, 140}}
                }
            }
        });
        op->associateInput(0,myInput);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        myAvgPool->forward();

        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("Stride >= feature dim") {
        std::shared_ptr<Tensor> myInput2 = std::make_shared<Tensor>(Array4D<float,1,1,3,3> { //NCHW
        {
            {
                {{0.3745, 0.9507, 0.7320},
                 {0.5987, 0.1560, 0.1560},
                 {0.0581, 0.8662, 0.6011}}
            }
        }
        });
        std::shared_ptr<Node> myAvgPool = AvgPooling({3,3}, "myAvgPool", {3,3});
        auto op = std::static_pointer_cast<OperatorTensor>(myAvgPool -> getOperator());

        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<float,1,1,1,1> {
            {{{{(0.3745 + 0.9507 + 0.7320 + 0.5987 + 0.1560 + 0.1560 + 0.0581 + 0.8662 + 0.6011)/9.0}}}}
        });
        op->associateInput(0,myInput2);
        op->setDataType(DataType::Float32);
        op->setBackend("cuda");
        myAvgPool->forward();

        float* computedOutput   = new float[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(float) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const float targetOutput = *(static_cast<float*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }

    SECTION("half") {
        std::shared_ptr<Tensor> myInput2 = std::make_shared<Tensor>(Array4D<half_float::half,1,1,3,3> { //NCHW
        {
            {
                {{half_float::half(0.3745), half_float::half(0.9507), half_float::half(0.7320)},
                 {half_float::half(0.5987), half_float::half(0.1560), half_float::half(0.1560)},
                 {half_float::half(0.0581), half_float::half(0.8662), half_float::half(0.6011)}}
            }
        }
        });
        myInput2->setBackend("cuda");

        std::shared_ptr<Node> myAvgPool = AvgPooling({3,3}, "mymyAvgPoolcdw", {3,3});
        auto op = std::static_pointer_cast<OperatorTensor>(myAvgPool -> getOperator());
        std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<half_float::half,1,1,1,1> {
            {{{{(half_float::half(0.3745) + half_float::half(0.9507) + half_float::half(0.7320) + half_float::half(0.5987) + half_float::half(0.1560) + half_float::half(0.1560) + half_float::half(0.0581) + half_float::half(0.8662) + half_float::half(0.6011))/half_float::half(9.0)}}}}
        });
        op->associateInput(0,myInput2);
        op->setDataType(DataType::Float16);
        op->setBackend("cuda");
        myAvgPool->forward();

        half_float::half* computedOutput   = new half_float::half[myOutput->size()]();
        cudaMemcpy(computedOutput, op->getOutput(0)->getImpl()->rawPtr(), sizeof(half_float::half) * myOutput->size(), cudaMemcpyDeviceToHost);

        for(int i = 0; i < myOutput->size(); i++){
            const half_float::half targetOutput = *(static_cast<half_float::half*>(myOutput->getImpl()->rawPtr()) + i);
            REQUIRE(fabs(computedOutput[i] - targetOutput) < 1e-6);
        }

        delete[] computedOutput;
    }
}
/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_FCIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_FCIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
class FCImplForward_cuda : public Registrable<FCImplForward_cuda,
                                                 std::tuple<DataType>,
                                                 void(std::size_t , std::size_t, std::size_t, bool, const void* , const void* , const void* , void*)> {};
class FCImpl_cuda : public OperatorImpl {
private:
    std::shared_ptr<Tensor> mInput0Fallback;
    std::shared_ptr<Tensor> mInput1Fallback;
    std::shared_ptr<Tensor> mInput2Fallback;


public:
    FCImpl_cuda(const FC_Op &op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<FCImpl_cuda> create(const FC_Op &op) {
        return std::make_unique<FCImpl_cuda>(op);
    }

public:
    void forward();
    // ~FCImpl_cuda();

private:
    template <class T> void forward_(const Tensor& input0, const Tensor& input1, const Tensor& input2, bool noBias, std::size_t outChannels);
};

namespace {
// add cuda backend to FC_Op implementation registry
static Registrar<FC_Op> registrarFCImpl_cuda("cuda", Aidge::FCImpl_cuda::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_FCIMPL_H_ */

/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_RELUIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_RELUIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
class ReLUImpl_cuda : public OperatorImpl {
private:
    // CuDNN specific variables
    #if CUDNN_VERSION >= 5000
        cudnnActivationDescriptor_t mReLUDesc = nullptr;
    #else
        cudnnActivationMode_t mReLUDesc = nullptr;
    #endif
    std::shared_ptr<Tensor> mInputFallback;

public:
    ReLUImpl_cuda(const ReLU_Op &op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<ReLUImpl_cuda> create(const ReLU_Op &op) {
        return std::make_unique<ReLUImpl_cuda>(op);
    }

public:
    void forward();
    ~ReLUImpl_cuda();

private:
    template <class T> void forward_(const Tensor& input);
};

namespace {
// add cuda backend to ReLU_Op implementation registry
static Registrar<ReLU_Op> registrarReLUImpl_cuda("cuda", Aidge::ReLUImpl_cuda::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_RELUIMPL_H_ */

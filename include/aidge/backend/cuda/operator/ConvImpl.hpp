/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_CONVIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_CONVIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
template <DimIdx_t DIM>
class ConvImpl_cuda : public OperatorImpl {
private:
    // CuDNN specific variables
    cudnnConvolutionDescriptor_t mConvDesc = nullptr;
    cudnnFilterDescriptor_t mFilterDesc = nullptr;
    cudnnConvolutionFwdAlgo_t mFwdAlgo;
    cudnnConvolutionBwdFilterAlgo_t mBwdFilterAlgo;
    cudnnConvolutionBwdDataAlgo_t mBwdDataAlgo;
    size_t mWorkspaceSize = 0;
    void* mFwdWorkspace = nullptr;
    void* mBwdWorkspace = nullptr;
    std::shared_ptr<Tensor> mInput0Fallback;
    std::shared_ptr<Tensor> mInput1Fallback;
    std::shared_ptr<Tensor> mInput2Fallback;

public:
    ConvImpl_cuda(const Conv_Op<DIM> &op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<ConvImpl_cuda> create(const Conv_Op<DIM> &op) {
        return std::make_unique<ConvImpl_cuda>(op);
    }

public:
    void forward();
    void backward();
    ~ConvImpl_cuda();

private:
    template <class T> void forward_(const Tensor& input0, const Tensor& input1, const Tensor& input2);
    template <class T> void backward_(const Tensor& input0, const Tensor& input1, const Tensor& input2);
};

namespace {
// add cuda backend to Conv_Op<2> implementation registry
static Registrar<Conv_Op<2>> registrarConvImpl_cuda("cuda", Aidge::ConvImpl_cuda<2>::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_CONVIMPL_H_ */

/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_BACKEND_CUDA_OPERATOR_MAXPOOLINGIMPL_H_
#define AIDGE_BACKEND_CUDA_OPERATOR_MAXPOOLINGIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include <cudnn.h>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/MaxPooling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cuda/utils/CudaUtils.hpp"

namespace Aidge {
template <DimIdx_t DIM>
class MaxPoolingImpl_cuda : public OperatorImpl {
private:
    // CuDNN specific variables
    cudnnPoolingDescriptor_t mMaxPoolingDesc = nullptr;
    cudnnPoolingMode_t mMode = CUDNN_POOLING_MAX;
    std::shared_ptr<Tensor> mInputFallback;

public:
    MaxPoolingImpl_cuda(const MaxPooling_Op<DIM> &op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<MaxPoolingImpl_cuda> create(const MaxPooling_Op<2> &op) {
        return std::make_unique<MaxPoolingImpl_cuda>(op);
    }

public:
    void forward();
    ~MaxPoolingImpl_cuda();

private:
    template <class T> void forward_(const Tensor& input);
};

namespace {
// add cuda backend to MaxPooling_Op<2> implementation registry
static Registrar<MaxPooling_Op<2>> registrarMaxPoolingImpl_cuda("cuda", Aidge::MaxPoolingImpl_cuda<2>::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_BACKEND_CUDA_OPERATOR_MAXPOOLINGIMPL_H_ */
